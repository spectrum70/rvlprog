                                        
# RVLPROG                               



## Introduction

Enjoy using your REVELPROG IS programmer on Linux.

This program is opensource, GPL2, take it as is.
Features will be added as per free time of the author. Contributors are welcome,
just contact the author by the email below in case.

This application is strictly linked with the REVELPROG IS firmware version.

## Build form sources

~~~
git clone git@gitlab.com:spectrum70/rvlprog.git

cd rvlprog
./configure
make 
sudo make install
~~~

## CONTACT

Please report issues to:

angelo@kernel-space.org


## NOTICE

The source code is Copyright (C) 2020

Angelo Dureghello (C) Kernelspace 2024, Trieste, Italy   
Reveltronics (C) 2020 - protocols and support


