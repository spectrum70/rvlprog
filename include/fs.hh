#ifndef fs_hh
#define fs_hh

#include <fstream>
#include <string>

using std::fstream;
using std::string;

namespace fs {
	int get_file_size(fstream &f);
	bool file_exist(const string &name);
};

#endif /* fs_hh */
