/*
 * This file is part of the "rvlprog" software.
 *
 * Copyright (C) 2020, Angelo Dureghello
 * Copyright (C) 2020, Reveltronics - provided protocol
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the
 * Free Software Foundation
 * 51 Franklin Street, Fifth Floor
 * Boston, MA  02110-1301, USA.
 */

#include <libusb-1.0/libusb.h>
#include <cassert>
#include <cstdint>
#include <fstream>
#include <algorithm>
#include <cstring>
#include <unistd.h>

#include "usb.hh"
#include "trace.hh"
#include "fs.hh"
#include "utils.hh"
#include "version.hh"

using std::cout;
using std::fstream;
using std::min;

static const int ep_out = 0x01;
static const int ep_out_data = 0x03;
static const int ep_in_ctrl = 0x81;
static const int ep_in_data = 0x82;

static const int max_blk_size = 16384;
static const int max_erase_timeout = 10;

static const char tmp_file_name[] = "/tmp/rvlprog";

usb::usb()
{
	setup_device_list();
}

usb::usb(libusb_context *context, libusb_device *device)
: ctx(context), dev(device)
{
	msg << "setting up devices database ...\n";

	setup_device_list();

	if (init()) {
		err << "cannot initialize device, exiting.\n";
		exit(-1);
	}

	if (setup_programmer()) {
		err << "cannot setup programmer, exiting.\n";
		exit(-1);
	}
}

usb::~usb()
{
	if (config)
		libusb_free_config_descriptor(config);
	if (dev_handle)
		libusb_close(dev_handle);
	if (ctx)
		libusb_exit(ctx);
}

void usb::display_device_list()
{
	int i = 0;

	cout << "Supported devices :\n";

	if (dlist.size()) {
		for (auto const& [key, val] : dlist) {
			cout << ++i << ". " << key << "\n";
		}
	}
}

int usb::init(void)
{
	int open_rc = libusb_open(dev, &dev_handle);

	if (open_rc == LIBUSB_ERROR_ACCESS ||
		open_rc == LIBUSB_ERROR_NO_DEVICE) {
		err << "access denied, "
		  << "please check to have proper access rghts.\n";
		exit(-1);
	}

	assert(open_rc == 0);

	int rc = libusb_get_active_config_descriptor(dev, &config);
	assert(rc == 0);

	int filter_interface = -1;

	for (int j = 0; j < config->bNumInterfaces; j++) {
		if (filter_interface >= 0 && j != filter_interface)
			continue;

		if (!attach(j)) {

			int r = libusb_claim_interface(dev_handle, j);
			if(r < 0) {
				err << "cannot claim interface.\n";
				exit(-1);
			}
			return 0;
		}
	}

	return -1;
}

int usb::attach(int interface)
{
	int active = libusb_kernel_driver_active(dev_handle, interface);

	cur_interface = interface;

	if (active != 0) {
		detach();
	}

	msg << "device attached to interface: " << interface << "\n";

	return 0;
}

int usb::detach()
{
	return libusb_detach_kernel_driver(dev_handle, cur_interface);
}

void usb::send_end_session()
{
	int len;

	setup_set_hw_vpp();
	libusb_bulk_transfer(dev_handle, ep_out, &buff[0], 32, &len, 0);
	setup_set_hw_bus();
	libusb_bulk_transfer(dev_handle, ep_out, &buff[0], 32, &len, 0);
}

int usb::setup_programmer()
{
	int len;

	setup_read_fw_ver();
	libusb_bulk_transfer(dev_handle, ep_out, &buff[0], 32, &len, 0);
	libusb_bulk_transfer(dev_handle, ep_in_ctrl, inbuff, 32, &len, 0);

	/* TO DO: is it a product code, or std reply ? */
	if (len != 4)
		return -1;

	trace::get().print("Revelprog IS fw version %c.%c.%c\n",
				inbuff[1] + 48,
				inbuff[2] + 48, inbuff[3] + 48);

	if (inbuff[1] + 48 != fw_ver_compat[0] ||
		inbuff[2] + 48 != fw_ver_compat[2] ||
		inbuff[3] + 48 != fw_ver_compat[4]) {
		err << "The current Revelprog IR firmware version is "
			"not compatible with this application. Please "
			"use the " << fw_ver_compat << " firmware version.\n";
		return -1;
	}

	return 0;
}

int usb::receive_target_content(libusb_device_handle *dev_handle,
					const string &out_file)
{
	int blocks, device_size;
	int len, count = 0;
	int bsize;
	fstream f(out_file.c_str(), fstream::binary | fstream::out);

	device_size = get_mem_size();
	bsize = get_blk_size();

	blocks = device_size / bsize;

	vt100::cursor_off();

	while (blocks--) {
		int chunk = bsize;
		unsigned char *p = inbuff;
		count++;
		while (chunk) {
			libusb_bulk_transfer(
				dev_handle, ep_in_data, p, bsize, &len, 0);

			if (len > 0 && len <= bsize) {
				p += len;
				chunk -= len;
			} else {
				break;
			}
			usleep(100);
		}
		/* Added in fw 1.9.1,
		 * meaning not clear
		 */
		if (!(count % 2)) {
			libusb_bulk_transfer(
				dev_handle, ep_in_ctrl, p, 6, &len, 0);
		}

		trace::get().update_progress(count);
		f.write((char *)inbuff, bsize);
	}

	vt100::cursor_on();

	libusb_bulk_transfer(dev_handle, ep_in_ctrl, inbuff, 3, &len, 0);
	if (len == 3 &&
		(inbuff[0] == 0x01 && inbuff[1] == 0xb2 && inbuff[2] == 0x00)) {
		trace::get().end_progress();
		send_end_session();
		return 0;
	}

	err << "transfer aborted\n";

	return 1;
}

int usb::read_sequence(const string &out_file)
{
	int len;

	setup_read();
	libusb_bulk_transfer(dev_handle, ep_out, &buff[0], 32, &len, 0);
	libusb_bulk_transfer(dev_handle, ep_in_ctrl, inbuff, 3, &len, 0);

	if (inbuff[0] != 0x01 || inbuff[1] != 0xb1 || inbuff[2] != 0) {
		err << "wrong reply, aborting.\n";
		return -1;
	}

	setup_sync();
	libusb_bulk_transfer(dev_handle, ep_out, &buff[0], 33, &len, 0);

	return receive_target_content(dev_handle, out_file);
}

/*
 * USB endpoints 0x00-0x7F are on the host
 * USB endpoints 0x80-0xFF are on the device
 * USB header
 * struct _URB_HEADER {
 * USHORT      Length;
 * USHORT      Function;
 * USBD_STATUS Status;
 * PVOID       UsbdDeviceHandle;
 * ULONG       UsbdFlags;
 * };
 */
int usb::read_binary(const string &out_file)
{
	int len;

	imp << "reading binary\n";

	setup_config();
	libusb_bulk_transfer(dev_handle, ep_out, &buff[0], 33, &len, 0);

	if (read_sequence(out_file) != 0)
		return -1;

	imp << "== completed ==\n";

	return 0;
}

int usb::erase_chip()
{
	int len;

	imp << "erasing memory ...\n";

	setup_config();
	libusb_bulk_transfer(dev_handle, ep_out, &buff[0], 33, &len, 0);

	/* common to different spi nor, likely a read */
	setup_erase();
	libusb_bulk_transfer(dev_handle, ep_out, &buff[0], 32, &len, 0);

	/* erase progress now, is sends 02 13 */
	int count = 0, to = 0;

	vt100::cursor_off();

	for (;;) {
		if (libusb_bulk_transfer(dev_handle,
			ep_in_ctrl, inbuff, 6, &len, 1000) ==
			LIBUSB_ERROR_TIMEOUT) {
			if (++to == max_erase_timeout) {
				vt100::cursor_on();
				err << "erase timeout, exiting.\n";
				return -1;
			}
			continue;
		}

		if (len == 3 && (inbuff[0] == 0x02 && inbuff[1] == 0x13)) {
			count++;
			trace::get().update_progress(count);
		} else if (len == 6) {
			if (inbuff[0] == 0x02 && inbuff[1] == 0x11 &&
			   *(uint32_t *)&inbuff[2] == 0xffffffff) {
				/* completed signal */
				trace::get().end_progress();
				imp << "== completed ==\n";
			 	break;
			} else if (inbuff[0] == 0x02 && inbuff[1] == 0x15 &&
			   *(uint32_t *)&inbuff[2] == 0) {
				/* informational message, we have to wait */
			}
		} else {
			vt100::cursor_on();
			err << "error erasing device, exiting.\n";
			return -1;
		}
	}

	vt100::cursor_on();

	return 0;
}

int usb::write_binary(const string &out_file)
{
	int len;
	int rval;

	if (erase_chip() != 0)
		return -1;

	/* New firmware needs time to setup properly */
	usleep(5000);

	imp << "writing memory ...\n";

	setup_write();
	rval = libusb_bulk_transfer(dev_handle, ep_out, &buff[0], 32, &len, 0);
	if (rval) {
		err << "send config, rval = " << rval << "\n";
		return -1;
	}

	setup_send_buff_start();
	rval = libusb_bulk_transfer(dev_handle, ep_out, &buff[0], 32, &len, 0);
	if (rval) {
		err << "send buff start, rval = " << rval << "\n";
		return -1;
	}

	/* we have to send 256 bytes blocks */
	fstream f(out_file.c_str(), fstream::binary | fstream::in);
	int size = get_mem_size();

	/* to do: check fsize is consinstent to the type */
	int chunks = size / 256;
	int count = 0, bcount = 0;

	if (!chunks) {
		err << "file seems too small, exiting\n";
		return -1;
	}

	vt100::cursor_off();

	/* Seems also 16384 blocks can be sent, not clear why they
	 * are visible only in some cases.
	 */
	while (chunks) {
		f.read((char *)obuff, 256);
		int to_write = 256;
		unsigned char *p = (unsigned char *)obuff;

		while (to_write) {
			rval = libusb_bulk_transfer(dev_handle,
				ep_out_data, p, to_write, &len, 10);
			if (!rval && len == to_write)
				break;
			if (rval != LIBUSB_ERROR_TIMEOUT) {
				err << "chhunk write, rval = " << rval << "\n";
				return -1;
			} else if (len < 256 && rval == LIBUSB_ERROR_TIMEOUT) {
				to_write -= len;
				p += len;
			} else if (rval) {
				err << "error writing block, val = "
					<< rval << "\n";
				return -1;
			}
			/* Added in fw 1.9.1:
			 * looks like after a partial write, device has
			 * something to say to proceed, so we need
			 * to discard this */
			libusb_bulk_transfer(dev_handle,
				ep_in_ctrl, inbuff, 6, &len, 10);
		}

		chunks--;
		count++;

		if (!(count % 64)) {
			bcount++;
			trace::get().update_progress(bcount);
		}
	}
	vt100::cursor_on();

	trace::get().end_progress();
	imp << "== completed ==\n";

	/* signal the end */
	setup_send_buff_stop();
	libusb_bulk_transfer(dev_handle, ep_out, &buff[0], 2, &len, 0);

	libusb_bulk_transfer(dev_handle,
			     ep_in_ctrl, inbuff, 6, &len, 10);
	libusb_bulk_transfer(dev_handle,
			     ep_in_ctrl, inbuff, 6, &len, 10);

	return 0;
}

int usb::verify_binary(const string &orig_binary)
{
	char buff1[max_blk_size + 1];
	char buff2[max_blk_size + 1];

	if (fs::file_exist(tmp_file_name))
		unlink(tmp_file_name);

	imp << "verifying ...\n";

	read_sequence(tmp_file_name);

	fstream f(tmp_file_name, fstream::binary | fstream::in);
	fstream g(orig_binary, fstream::binary | fstream::in);

	int size = min(get_mem_size(), fs::get_file_size(g));

	int bsize = get_blk_size();
	int blocks = size / bsize;
	int count;

	vt100::cursor_off();

	for (count = 0; count < blocks; count++) {
		if (f.eof() || g.eof())
			break;
		f.read(buff1, bsize);
		g.read(buff2, bsize);

		if (memcmp(buff1, buff2, bsize) != 0) {
			err << "error comparing data, block " <<
				count << ", exiting.\n";
			return -1;
		}
		msg << "\x1b[010" << progress[count % 4];
	}

	vt100::cursor_on();

	imp << "== completed ==\n";

	unlink(tmp_file_name);

	send_end_session();

	return 0;
}
