/*
 * This file is part of the "rvlprog" software.
 *
 * Copyright (C) 2020, Angelo Dureghello
 * Copyright (C) 2020, Reveltronics - provided protocol
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the
 * Free Software Foundation
 * 51 Franklin Street, Fifth Floor
 * Boston, MA  02110-1301, USA.
 */

#include <libusb-1.0/libusb.h>
#include <cassert>
#include <cstdio>

#include "usb_init.hh"
#include "usb_db.hh"
#include "trace.hh"

usb_init::usb_init() {}

/*
 * Endpoints 0 are reserved in every device, for control.
 * A USB device can have up to 32 endpoints (16 OUT and 16 IN).
 */
void usb_init::show_dev_info(libusb_device *dev, libusb_device_descriptor *desc)
{
	trace::get().print("usb device: %04x:%04x\n",
		desc->idVendor, desc->idProduct);

#ifdef DEBUG_DEVICE_DETAILS
	libusb_config_descriptor *config;

	libusb_get_config_descriptor(dev, 0, &config);

	dbg << "interfaces: " << (int)config->bNumInterfaces << "\n";

	const libusb_interface *inter;
	const libusb_interface_descriptor *interdesc;
 	const libusb_endpoint_descriptor *epdesc;
	for(int i = 0; i < (int)config->bNumInterfaces; i++) {
		inter = &config->interface[i];
		dbg << "alternate settings: "
			<< inter->num_altsetting << "\n";

		for(int j = 0; j < inter->num_altsetting; j++) {

			interdesc = &inter->altsetting[j];

			dbg << "interface :" <<
				(int)interdesc->bInterfaceNumber << "\n";
			dbg << "endpoints: " <<
				(int)interdesc->bNumEndpoints << "\n";

			for (int k = 0; k < interdesc->bNumEndpoints; k++) {
				epdesc = &interdesc->endpoint[k];
				dbg << "desctype: "
					<< epdesc->bDescriptorType << "\n";
				dbg << "ep addr: "
					<< epdesc->bEndpointAddress << "\n";
			}
		}
	}

	libusb_free_config_descriptor(config);
#endif
}

int usb_init::detect()
{
	libusb_device **list = NULL;
	int rc, rval;

	rc = libusb_init(&ctx);
	assert(rc == 0);

	libusb_get_device_list(ctx, &list);

	for (size_t idx = 0; list[idx] != NULL; idx ++) {
		libusb_device *dev = list[idx];
		libusb_device_descriptor desc = {0};

		int rc = libusb_get_device_descriptor(dev, &desc);
		assert(rc == 0);

		if (desc.idVendor  == reveltronics &&
			desc.idProduct == revelprog_is) {
			imp <<  "interface detected: Revelprog IS\n";

			show_dev_info(dev, &desc);

			device = dev;
			rval = 0;
			goto exit;
		}
	}

	rval = -1;
	err << "device not found, sorry.\n";

exit:
	libusb_free_device_list(list, 1);

	return rval;
}
