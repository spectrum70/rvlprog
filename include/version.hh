#ifndef version_hh
#define version_hh

static constexpr char version[] = "0.93";
static constexpr char fw_ver_compat[6] = {"1.9.1"};

#endif /* version_hh */
