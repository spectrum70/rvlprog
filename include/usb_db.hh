#ifndef usb_db_hh
#define usb_db_hh

/*
 * Reveltronics is presenting us as:
 * Bus 001 Device 097: ID 0483:5751 STMicroelectronics
 */
enum vendor {
	reveltronics = 0x0483,
};

enum device {
	revelprog_is = 0x5751,
};

#endif /* usb_db_hh */
