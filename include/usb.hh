#ifndef usb_hh
#define usb_hh

#include "protocol.hh"

struct libusb_context;
struct libusb_device;
struct libusb_device_handle;

using std::tuple;
using std::string;

static constexpr int max_in_buff = 65535;
static constexpr int max_out_buff = 65535;

struct usb : public protocol {
	usb();
	usb(libusb_context *ctx, libusb_device *device);
	~usb();

	int erase_chip();
	int read_binary(const string &out_file);
	int write_binary(const string &out_file);
	int verify_binary(const string &out_file);
	void display_device_list();
private:
	int init(void);
	int setup_programmer();
	int attach(int interface);
	int detach();
	int read_sequence(const string &out_file = "");
	void send_end_session();
	int receive_target_content(libusb_device_handle *dev_handle,
			const string &out_file);
private:
	libusb_context *ctx = {};
	libusb_device *dev;
	struct libusb_device_handle *dev_handle = {};
	struct libusb_config_descriptor *config = {};
	int cur_interface;

	typedef void (*func)(libusb_device_handle *dev_handle);

	unsigned char inbuff[max_in_buff];
	unsigned char obuff[max_out_buff];
};

#endif /* usb_hh */
