#ifndef getopts_hh
#define getopts_hh

#include <string>
#include <vector>

using std::string;
using std::vector;

struct opts {
	string type;
	bool read;
	bool write;
	bool verify;
	bool list;
	bool erase;
	vector<string> nonopts {};
};

struct getopts {
	getopts(int argc, char **argv);
	opts & get_options() { return options; }
	void info();
	void usage();
private:
	opts options {};
};

#endif /* getopts_hh */
