/*
 * This file is part of the "rvlprog" software.
 *
 * Copyright (C) 2020, Angelo Dureghello
 * Copyright (C) 2020, Reveltronics - provided protocol
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the
 * Free Software Foundation
 * 51 Franklin Street, Fifth Floor
 * Boston, MA  02110-1301, USA.
 */
#include "utils.hh"

#include <sstream>
#include <iomanip>

using namespace std;

namespace utils {
	
string itoa(int val)
{
	stringstream ss;

	ss << val;

	return ss.str();
}

string itoaz(int val, int zpad)
{
	stringstream ss;

	ss << dec << setw(zpad) << setfill('0') << val;

	return ss.str();
}

};