#ifndef usb_init_hh
#define usb_init_hh

struct libusb_context;
struct libusb_device;
struct libusb_device_descriptor;

struct usb_init {
	usb_init();
	int detect();
	libusb_context * get_ctx() { return ctx; }
	libusb_device * get_device() { return device; }
private:
	void show_dev_info(libusb_device *dev, libusb_device_descriptor *desc);
private:
	int desc;
	libusb_context *ctx;
	libusb_device *device;
};

#endif /* usb_init_hh */
